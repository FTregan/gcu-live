#!/usr/bin/env python3.9

import json
import re
import datetime
import ftfy
import configparser
import os
import sys

msg = re.compile(r'\[(.+?)\] <(.+?)> (.+)')
action = re.compile(r'\[(.+?)\] Action: ([^ ]+) (.+)')

log_lines = []
now = datetime.datetime.now()
lines = []
urlrx = r'.*((https?://)+[^\ \n$]+)'

config = configparser.ConfigParser()
config.read(os.path.join(os.path.expanduser("~"), "etc", "irclog2json.ini"))
ircpath = config['pinpin']['ircpath']
wwwpath = config['pinpin']['wwwpath']
jsonfile = config['pinpin']['jsonfile']

if len(sys.argv) > 1:
    arcdate = sys.argv[1]
    ircpath = '{}.{}'.format(ircpath, arcdate)
    wwwpath = '{}/{}/{}.{}.json'.format(wwwpath, 'history', jsonfile, arcdate)
else:
    ircpath = '{}.{}'.format(ircpath, now.strftime("%d%b%Y"))
    wwwpath = '{}/{}.json'.format(wwwpath, jsonfile)

with open(ircpath, 'rb') as f:
    for line in f:
        (str, encoding) = ftfy.guess_bytes(line)
        line = line.decode(encoding, errors='ignore')
        lines.append(line)

        if re.search(r'[\[#]\ ?nolog\ ?[#\]]?', line):
            continue

        msgmatch = msg.match(line)
        actionmatch = action.match(line)

        if msgmatch:
            time, nickname, phrase = msgmatch.groups()
            act = False
        elif actionmatch:
            time, nickname, phrase = actionmatch.groups()
            act = True
        else:
            continue

        log_line = {
            'time': time,
            'nickname': nickname,
            'phrase': phrase,
            'action': act
        }
 
        match = re.match(urlrx, phrase)
        if match is not None:
            log_line['url'] = match.group(1)
    
        log_lines.append(log_line)

    log_lines[-1]['last'] = True


json_data = json.dumps(log_lines, indent = 4, sort_keys = True)

with open(wwwpath, 'w', encoding='utf-8') as outfile:
    outfile.write(json_data)

